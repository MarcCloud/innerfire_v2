package com.sahuarogames.innerfire.sceneresources;

import com.sahuarogames.innerfire.gameresources.Image;

public abstract class SceneResources {
	public abstract void loadResources();

	public abstract void unloadResources();
	
	public abstract Image getImage(Class<? extends Image> imageClass);
}
