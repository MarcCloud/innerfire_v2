package com.sahuarogames.innerfire.sceneresources;

import org.andengine.audio.music.MusicFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;

import com.sahuarogames.innerfire.gameresources.Image;

public class LoadingSceneResources extends SceneResources {
	@Override
	public void loadResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		MusicFactory.setAssetBasePath("snd/");
	}

	@Override
	public Image getImage(Class<? extends Image> imageClass) {
		return null;
	}

	@Override
	public void unloadResources() {
	}
}
