package com.sahuarogames.innerfire.sceneresources;

import com.sahuarogames.innerfire.gameresources.Image;
import com.sahuarogames.innerfire.images.CharacterSelectionSceneStartGameImage;

public class CharacterSelectionSceneResources extends SceneResources {
	private CharacterSelectionSceneStartGameImage startGameImage;
	
	@Override
	public void loadResources() {
		this.startGameImage = new CharacterSelectionSceneStartGameImage();
	}

	@Override
	public void unloadResources() {
		this.startGameImage.unload();
	}

	@Override
	public Image getImage(Class<? extends Image> imageClass) {
		if (imageClass == CharacterSelectionSceneStartGameImage.class) {
			return this.startGameImage;
		}

		return null;
	}
}
