package com.sahuarogames.innerfire.sceneresources;

import com.sahuarogames.innerfire.gameresources.Image;
import com.sahuarogames.innerfire.images.PresentationSceneBackgroundImage;
import com.sahuarogames.innerfire.images.PresentationSceneStartOptionImage;

public class PresentationSceneResources extends SceneResources {
	private PresentationSceneBackgroundImage backgroundImage;
	private PresentationSceneStartOptionImage startOptionImage;

	@Override
	public void loadResources() {
		this.backgroundImage = new PresentationSceneBackgroundImage();
		this.startOptionImage = new PresentationSceneStartOptionImage();
	}

	@Override
	public Image getImage(Class<? extends Image> imageClass) {
		if (imageClass == PresentationSceneBackgroundImage.class) {
			return this.backgroundImage;
		} else if (imageClass == PresentationSceneStartOptionImage.class) {
			return this.startOptionImage;
		}

		return null;
	}

	@Override
	public void unloadResources() {
		this.backgroundImage.unload();
		this.startOptionImage.unload();
	}
}
