package com.sahuarogames.innerfire.images;

import com.sahuarogames.innerfire.gameresources.Image;

public class PresentationSceneBackgroundImage extends Image {
	private static final String GFX_PATH = "gfx/presentation_scene/";
	private static final String FILE_NAME = "background.png";
	private static final int ATLAS_WIDTH = 1024;
	private static final int ATLAS_HEIGHT = 1024;
	
	public PresentationSceneBackgroundImage() {
		super(GFX_PATH, FILE_NAME, ATLAS_WIDTH, ATLAS_HEIGHT);
	}
}
