package com.sahuarogames.innerfire.images;

import com.sahuarogames.innerfire.gameresources.Image;

public class PresentationSceneStartOptionImage extends Image {
	private static final String GFX_PATH = "gfx/presentation_scene/";
	private static final String FILE_NAME = "startOption.png";
	private static final int ATLAS_WIDTH = 512;
	private static final int ATLAS_HEIGHT = 512;

	public PresentationSceneStartOptionImage() {
		super(GFX_PATH, FILE_NAME, ATLAS_WIDTH, ATLAS_HEIGHT);
	}
}
