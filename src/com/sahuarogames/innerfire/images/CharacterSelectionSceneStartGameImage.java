package com.sahuarogames.innerfire.images;

import com.sahuarogames.innerfire.gameresources.Image;

public class CharacterSelectionSceneStartGameImage extends Image {
	private static final String GFX_PATH = "gfx/character_selection_scene/";
	private static final String FILE_NAME = "startGame.png";
	private static final int ATLAS_WIDTH = 256;
	private static final int ATLAS_HEIGHT = 256;

	public CharacterSelectionSceneStartGameImage() {
		super(GFX_PATH, FILE_NAME, ATLAS_WIDTH, ATLAS_HEIGHT);
	}
}
