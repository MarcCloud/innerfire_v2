package com.sahuarogames.innerfire.images;

import com.sahuarogames.innerfire.gameresources.Image;

public class BigFullBodyImage extends Image {
	private static final String FILE_NAME = "bigFullBody.png";
	private static final int ATLAS_WIDTH = 512;
	private static final int ATLAS_HEIGHT = 512;

	public BigFullBodyImage(String path) {
		super(path, FILE_NAME, ATLAS_WIDTH, ATLAS_HEIGHT);
	}
}
