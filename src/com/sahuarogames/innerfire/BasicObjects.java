package com.sahuarogames.innerfire;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.sahuarogames.innerfire.characters.GameCharacter;

public class BasicObjects {
	public static Engine engine;
	public static GameActivity activity;
	public static BoundCamera camera;
	public static VertexBufferObjectManager vbom;
	public static GameCharacter character;
}