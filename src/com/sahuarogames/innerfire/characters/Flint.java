package com.sahuarogames.innerfire.characters;

import com.sahuarogames.innerfire.gameresources.CharacterResources;
import com.sahuarogames.innerfire.R;

public class Flint extends GameCharacter {
	public Flint() {
		this.resources = new CharacterResources(R.array.Flint);
	}
}