package com.sahuarogames.innerfire.characters;

import com.sahuarogames.innerfire.gameresources.CharacterResources;
import com.sahuarogames.innerfire.R;

public class Kory extends GameCharacter {
	public Kory() {
		this.resources = new CharacterResources(R.array.Kory);
	}
}