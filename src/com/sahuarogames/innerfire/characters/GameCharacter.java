package com.sahuarogames.innerfire.characters;

import com.sahuarogames.innerfire.R;
import com.sahuarogames.innerfire.gameresources.CharacterResources;

public abstract class GameCharacter {
	public CharacterResources resources;
	
	public static GameCharacter Get(int code) {
		switch (code) {
		case R.array.Flint:
			return new Flint();
		case R.array.Kory:
			return new Kory();
		}
		
		return null;
	}
}
