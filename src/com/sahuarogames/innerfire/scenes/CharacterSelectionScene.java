package com.sahuarogames.innerfire.scenes;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import com.sahuarogames.innerfire.BasicObjects;
import com.sahuarogames.innerfire.gameresources.Fonts;
import com.sahuarogames.innerfire.gameresources.Image;
import com.sahuarogames.innerfire.gameresources.ImageTouchListener;
import com.sahuarogames.innerfire.gameresources.Musics;
import com.sahuarogames.innerfire.gameresources.Strings;
import com.sahuarogames.innerfire.images.CharacterSelectionSceneStartGameImage;
import com.sahuarogames.innerfire.scenelayers.CharacterSelectionSceneBackLayer;
import com.sahuarogames.innerfire.sceneresources.SceneResourcesFactory;

import com.sahuarogames.innerfire.R;

public class CharacterSelectionScene extends BaseScene implements ImageTouchListener {
	private final static String CHARACTER_SELECT_TITLE_TEXT = Strings
			.get(R.string.presentationScene_characterSelectTitle);

	private CharacterSelectionSceneBackLayer backLayer;

	private Text characterSelectTitleText;

	private Rectangle flintOptionRectangle;
	private Rectangle koryOptionRectangle;
	private Rectangle nikkOptionRectangle;
	private Rectangle momoriOptionRectangle;

	public CharacterSelectionScene() {
		this.sceneResources = SceneResourcesFactory.getResources(this.getClass());

		this.backLayer = new CharacterSelectionSceneBackLayer();
		this.backLayer.loadAndDisplayCharacterResources(R.array.Flint);
		this.attachChild(this.backLayer);

		this.displayCharacterSelectTitleText();
		this.displayCharacterSelectionOptions();
		this.displayStartGameSprite();
		Musics.startJarabeTapatio(false);
	}

	private void displayCharacterSelectTitleText() {
		this.characterSelectTitleText = new Text(10, 42, Fonts.BIG_TITLE, CHARACTER_SELECT_TITLE_TEXT,
				BasicObjects.vbom);
		this.attachChild(this.characterSelectTitleText);
	}

	private void displayStartGameSprite() {
		Image startGameImage = this.sceneResources.getImage(CharacterSelectionSceneStartGameImage.class);
		Sprite startGameSprite = startGameImage.createSprite(BasicObjects.camera.getXMax() - startGameImage.width - 6,
				BasicObjects.camera.getYMax() - startGameImage.height - 10, this);
		this.registerTouchArea(startGameSprite);
		this.attachChild(startGameSprite);
	}

	private void displayCharacterSelectionOptions() {
		this.attachOptionRectangle(this.flintOptionRectangle, 355, 210, R.array.Flint);
		this.attachOptionRectangle(this.koryOptionRectangle, 455, 170, R.array.Kory);
		this.attachOptionRectangle(this.nikkOptionRectangle, 355, 290, R.array.Flint);
		this.attachOptionRectangle(this.momoriOptionRectangle, 455, 250, R.array.Kory);
	}

	private void attachOptionRectangle(Rectangle optionRectangle, int pX, int pY, final int characterCode) {
		optionRectangle = new Rectangle(pX, pY, 60, 60, BasicObjects.vbom) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionUp() && backLayer.getCurrentCharacter().getCode() != characterCode) {
					backLayer.replaceCurrentCharacterInfo(characterCode);
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		optionRectangle.setColor(Color.WHITE);

		this.registerTouchArea(optionRectangle);
		this.attachChild(optionRectangle);
	}

	@Override
	public void onBackButtonPressed() {
		ScenesManager.displayScene(PresentationScene.class);
	}

	@Override
	public void clear() {
		this.detachChildren();
		this.sceneResources.unloadResources();
		this.backLayer.dispose();
		Musics.pauseMusic();
	}

	@Override
	public void onImageTouched(Image touchedImage, TouchEvent pSceneTouchEvent) {
		if (touchedImage.equals(this.sceneResources.getImage(CharacterSelectionSceneStartGameImage.class))) {
			if (pSceneTouchEvent.isActionUp()) {
				ScenesManager.displayScene(PresentationScene.class);
			}
		}
	}
}