package com.sahuarogames.innerfire.scenes;

import com.sahuarogames.innerfire.sceneresources.SceneResourcesFactory;

public class LoadingScene extends BaseScene {
	public LoadingScene() {
		this.sceneResources = SceneResourcesFactory.getResources(this.getClass());
	}

	@Override
	public void onBackButtonPressed() {
	}

	@Override
	public void clear() {
	}
}
