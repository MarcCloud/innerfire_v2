package com.sahuarogames.innerfire.scenes;

import com.sahuarogames.innerfire.BasicObjects;

public class ScenesManager {
	public static BaseScene currentScene;

	public static <T extends BaseScene> void displayScene(Class<T> c) {
		if (currentScene != null) {
			currentScene.clear();
		}
		
		BaseScene loadingScene = new LoadingScene();
		BasicObjects.engine.setScene(loadingScene);

		try {
			T scene = c.newInstance();
			BasicObjects.engine.setScene(scene);
			currentScene = scene;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}