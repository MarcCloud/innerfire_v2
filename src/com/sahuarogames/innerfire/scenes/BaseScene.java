package com.sahuarogames.innerfire.scenes;

import org.andengine.entity.scene.Scene;

import com.sahuarogames.innerfire.sceneresources.SceneResources;

public abstract class BaseScene extends Scene {
	public SceneResources sceneResources;

	public abstract void onBackButtonPressed();
	
	public abstract void clear();
}
