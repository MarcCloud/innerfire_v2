package com.sahuarogames.innerfire.scenes;

import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;

import com.sahuarogames.innerfire.BasicObjects;
import com.sahuarogames.innerfire.gameresources.Image;
import com.sahuarogames.innerfire.gameresources.ImageTouchListener;
import com.sahuarogames.innerfire.gameresources.Musics;
import com.sahuarogames.innerfire.images.PresentationSceneBackgroundImage;
import com.sahuarogames.innerfire.images.PresentationSceneStartOptionImage;
import com.sahuarogames.innerfire.sceneresources.SceneResourcesFactory;

public class PresentationScene extends BaseScene implements ImageTouchListener {
	private Sprite backgroundSprite;
	private Sprite startOptionSprite;

	public PresentationScene() {
		this.sceneResources = SceneResourcesFactory.getResources(this.getClass());
		this.showBackground();
		this.showStartOption();
		Musics.startJarabeTapatio(true);
	}

	private void showStartOption() {
		Image startOptionImage = this.sceneResources.getImage(PresentationSceneStartOptionImage.class);
		this.startOptionSprite = startOptionImage.createSprite(BasicObjects.camera.getCenterX()
				- startOptionImage.width / 2f, 300, this);
		this.registerTouchArea(this.startOptionSprite);
		this.attachChild(this.startOptionSprite);
	}

	private void showBackground() {
		Image backgroundImage = this.sceneResources.getImage(PresentationSceneBackgroundImage.class);
		this.backgroundSprite = backgroundImage.createSprite(0, 0);
		this.setBackground(new SpriteBackground(this.backgroundSprite));
	}

	@Override
	public void onBackButtonPressed() {
		System.exit(0);
	}

	@Override
	public void clear() {
		this.detachChildren();
		this.sceneResources.unloadResources();
	}

	@Override
	public void onImageTouched(Image touchedImage, TouchEvent pSceneTouchEvent) {
		if (touchedImage.equals(this.sceneResources.getImage(PresentationSceneStartOptionImage.class))) {
			if (pSceneTouchEvent.isActionUp()) {
				ScenesManager.displayScene(CharacterSelectionScene.class);
			}
		}
	}
}
