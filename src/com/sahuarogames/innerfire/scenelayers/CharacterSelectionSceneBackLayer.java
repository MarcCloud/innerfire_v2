package com.sahuarogames.innerfire.scenelayers;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;

import android.util.SparseArray;

import com.sahuarogames.innerfire.BasicObjects;
import com.sahuarogames.innerfire.gameresources.Fonts;
import com.sahuarogames.innerfire.gameresources.CharacterResources;
import com.sahuarogames.innerfire.gameresources.Image;

public class CharacterSelectionSceneBackLayer extends SceneLayer {
	public int currentCharacterCode;
	
	private Rectangle characterSelectTitleBackgroundRectangle;
	private Text nameText;
	private Text profileText;

	private SparseArray<CharacterResources> charactersResources = new SparseArray<CharacterResources>();
	private SparseArray<Sprite> characterSprites = new SparseArray<Sprite>();
	
	private Sprite bigFullBodySprite;
	
	public void replaceCurrentCharacterInfo(int characterCode) {
		this.detachChildren();
		this.loadAndDisplayCharacterResources(characterCode);
	}
	
	public void loadAndDisplayCharacterResources(int characterCode) {
		this.currentCharacterCode = characterCode;
		this.displayTitle();
		this.displayBigFullBody();
		this.displayCharacterTexts();
	}
	
	private void displayTitle() {
		this.characterSelectTitleBackgroundRectangle = new Rectangle(0, 30, 800, 80, BasicObjects.vbom);
		this.characterSelectTitleBackgroundRectangle.setColor(this.getCurrentCharacter().getMainColor());
		this.attachChild(characterSelectTitleBackgroundRectangle);
	}
	
	public CharacterResources getCurrentCharacter() {
		if (this.charactersResources.indexOfKey(this.currentCharacterCode) < 0) {
			CharacterResources currentCharacterResources = new CharacterResources(this.currentCharacterCode);
			this.charactersResources.put(this.currentCharacterCode, currentCharacterResources);
		}
			
		return this.charactersResources.get(this.currentCharacterCode);
	}
	
	private void displayBigFullBody() {		
		if (this.characterSprites.indexOfKey(this.currentCharacterCode) < 0) {
			Image bigFullBodyImage = this.getCurrentCharacter().getBigFullBodyImage();
			Sprite bigFullBodySprite = bigFullBodyImage.createSprite(
					BasicObjects.camera.getWidth() - bigFullBodyImage.width,
					BasicObjects.camera.getHeight() - bigFullBodyImage.height);
			this.characterSprites.put(this.currentCharacterCode, bigFullBodySprite);
		}
	
		this.bigFullBodySprite = this.characterSprites.get(this.currentCharacterCode);
		this.attachChild(this.bigFullBodySprite);
	}
	
	private void displayCharacterTexts() {
		this.nameText = new Text(10, 125, Fonts.TITLE, this.getCurrentCharacter().getName(), BasicObjects.vbom);
		this.attachChild(this.nameText);

		this.profileText = new Text(10, 170, Fonts.NORMAL, this.getCurrentCharacter().getProfile(), new TextOptions(
				AutoWrap.WORDS, 280), BasicObjects.vbom);
		this.attachChild(this.profileText);
	}

	@Override
	public void dispose() {
		int s = this.charactersResources.size();
		for(int i = 0; i < s; i++) {
		    this.charactersResources.valueAt(i).unload();
		}
	}
}