package com.sahuarogames.innerfire.scenelayers;

import org.andengine.entity.Entity;

public abstract class SceneLayer extends Entity {
	public abstract void dispose();
}
