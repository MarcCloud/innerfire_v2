package com.sahuarogames.innerfire.gameresources;

import org.andengine.input.touch.TouchEvent;

public interface ImageTouchListener {
	public void onImageTouched(Image touchedImage, TouchEvent pSceneTouchEvent);
}
