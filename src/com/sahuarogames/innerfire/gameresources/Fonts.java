package com.sahuarogames.innerfire.gameresources;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.FontManager;
import org.andengine.util.color.Color;

import android.graphics.Typeface;

import com.sahuarogames.innerfire.BasicObjects;

public class Fonts {
	public static final Font NORMAL = loadFont(22, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
	public static final Font TITLE = loadFont(35, Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
	public static final Font BIG_TITLE = loadFont(50, Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

	private static Font loadFont(int size, Typeface typeface) {
		FontManager fontManager = BasicObjects.engine.getFontManager();
		Font font = FontFactory.create(fontManager, BasicObjects.engine.getTextureManager(), 256, 256, typeface, size,
				Color.WHITE_ABGR_PACKED_INT);
		font.load();
		fontManager.loadFont(font);
		return font;
	}
}
