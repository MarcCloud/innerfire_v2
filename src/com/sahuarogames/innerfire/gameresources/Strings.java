package com.sahuarogames.innerfire.gameresources;

import android.content.res.Resources;

import com.sahuarogames.innerfire.BasicObjects;

public class Strings {
	public static String get(int stringId) {
		Resources resources = BasicObjects.activity.getResources();
		return resources.getString(stringId);
	}
}
