package com.sahuarogames.innerfire.gameresources;

import android.content.res.Resources;

import com.sahuarogames.innerfire.BasicObjects;
import com.sahuarogames.innerfire.images.BigFullBodyImage;

import org.andengine.util.color.Color;

public class CharacterResources {
	private int code;
	private String[] stringsArray;
	private String gfxPath;

	private BigFullBodyImage bigFullBodyImage;

	public CharacterResources(int characterCode) {
		this.code = characterCode;
		Resources resources = BasicObjects.activity.getResources();
		this.stringsArray = resources.getStringArray(this.code);
		this.gfxPath = "gfx/characters/" + this.getName() + "/";
	}

	public void unload() {
		this.bigFullBodyImage.unload();
	}

	public Image getBigFullBodyImage() {
		if (this.bigFullBodyImage == null) {
			this.bigFullBodyImage = new BigFullBodyImage(this.gfxPath);
		}

		return this.bigFullBodyImage;
	}

	public String getName() {
		return this.stringsArray[0];
	}

	public CharSequence getProfile() {
		return this.stringsArray[1];
	}

	public Color getMainColor() {
		String[] colorValues = this.stringsArray[2].split(",");
		float r = Float.parseFloat(colorValues[0].trim());
		float g = Float.parseFloat(colorValues[1].trim());
		float b = Float.parseFloat(colorValues[2].trim());
		float a = Float.parseFloat(colorValues[3].trim());
		return new Color(r, g, b, a);
	}

	public int getCode() {
		return this.code;
	}
}