package com.sahuarogames.innerfire.gameresources;

import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;

import com.sahuarogames.innerfire.BasicObjects;

public abstract class Image {
	private BitmapTextureAtlas atlas;
	private TextureRegion region;
	
	private Sprite sprite;
	
	public float width;
	public float height;
	
	public Image(String path, String fileName, int atlasWidth, int atlasHeight) {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(path);
		
		this.atlas = new BitmapTextureAtlas(BasicObjects.activity.getTextureManager(), atlasWidth, atlasHeight);
		this.region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.atlas, BasicObjects.activity, fileName, 0, 0);
		
		this.width = this.region.getWidth();
		this.height = this.region.getHeight();
	}
	
	private void initializeSprite() {
		if (this.sprite == null) {
			this.atlas.load();
		} else {
			this.sprite.dispose();
		}
	}
	
	public Sprite createSprite(float pX, float pY) {
		this.initializeSprite();
		this.sprite = new Sprite(pX, pY, this.region, BasicObjects.vbom);
		return this.sprite;
	}
	
	public Sprite createSprite(float pX, float pY, final ImageTouchListener listener) {
		final Image touchedImage = this;
		this.initializeSprite();
		this.sprite = new Sprite(pX, pY, this.region, BasicObjects.vbom) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				listener.onImageTouched(touchedImage, pSceneTouchEvent);
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		return this.sprite;
	}
	
	public void unload() {
		this.atlas.unload();
	}
}