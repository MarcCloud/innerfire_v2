package com.sahuarogames.innerfire.gameresources;

import java.io.IOException;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;

import com.sahuarogames.innerfire.BasicObjects;

public class Musics {
	private static Music backgroundMusic;
	private static String currentMusicFile;

	private static void startMusic(String musicFile, boolean restartIfCurrent) {
		if (!restartIfCurrent && currentMusicFile == musicFile && backgroundMusic.isPlaying()) {
			return;
		}

		if (restartIfCurrent || currentMusicFile != musicFile) {
			if (backgroundMusic != null) {
				backgroundMusic.pause();
				backgroundMusic.release();
			}

			try {
				MusicFactory.setAssetBasePath("snd/music/");
				backgroundMusic = MusicFactory.createMusicFromAsset(BasicObjects.activity.getMusicManager(),
						BasicObjects.activity, musicFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		backgroundMusic.play();
		currentMusicFile = musicFile;
	}

	public static void pauseMusic() {
		if (backgroundMusic != null) {
			backgroundMusic.pause();
		}
	}

	public static void resumeMusic() {
		if (backgroundMusic != null) {
			backgroundMusic.play();
		}
	}

	public static void startJarabeTapatio(boolean restartIfCurrent) {
		startMusic("jarabe_tapatio.mp3", restartIfCurrent);
	}
}
