package com.sahuarogames.innerfire;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import com.sahuarogames.innerfire.gameresources.Musics;
import com.sahuarogames.innerfire.scenes.PresentationScene;
import com.sahuarogames.innerfire.scenes.ScenesManager;

import android.util.Log;
import android.view.KeyEvent;

public class GameActivity extends BaseGameActivity {
	private final static int CAMERA_WIDTH = 800;
	private final static int CAMERA_HEIGHT = 480;

	private BoundCamera camera;

	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) {
		return new LimitedFPSEngine(pEngineOptions, 60);
	}

	@Override
	public EngineOptions onCreateEngineOptions() {
		this.camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		EngineOptions options = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(
				CAMERA_WIDTH, CAMERA_HEIGHT), camera);
		options.getTouchOptions().setNeedsMultiTouch(true);
		options.getAudioOptions().setNeedsMusic(true);
		options.getAudioOptions().setNeedsSound(true);
		return options;
	}

	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) {
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
		BasicObjects.engine = this.mEngine;
		BasicObjects.activity = this;
		BasicObjects.camera = this.camera;
		BasicObjects.vbom = this.getVertexBufferObjectManager();

		ScenesManager.displayScene(PresentationScene.class);
		pOnCreateSceneCallback.onCreateSceneFinished(ScenesManager.currentScene);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			ScenesManager.currentScene.onBackButtonPressed();
		} else if (keyCode == KeyEvent.KEYCODE_HOME) {
			BasicObjects.engine.stop();
		}

		return false;
	}

	@Override
	protected void onPause() {
		Musics.pauseMusic();
		Log.e("INNER FIRE", "Paused music from GameActivity.");
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Musics.resumeMusic();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (this.isGameLoaded()) {
			System.exit(0);
		}
	}

	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) {
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
}
