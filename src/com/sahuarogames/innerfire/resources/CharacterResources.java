package com.sahuarogames.innerfire.resources;

import android.content.res.Resources;

import com.sahuarogames.innerfire.BasicObjects;
import com.sahuarogames.innerfire.CharacterNames;
import com.sahuarogames.innerfire.R;

import org.andengine.audio.music.MusicFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;

public class CharacterResources {
	private String character;
	private String[] stringsArray;
	
	public CharacterResources(String character) {
		this.character = character;
		
		Resources resources = BasicObjects.activity.getResources();
		if (character == CharacterNames.FLINT) {
			this.stringsArray = resources.getStringArray(R.array.flint);
		}
	}
	
	public void loadResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/characters/" + this.character + "/");
		MusicFactory.setAssetBasePath("snd/presentation_scene/characters/" + this.character + "/");
	}
	
	public String getText(int position) {
		return this.stringsArray[position];
	}

	public void unloadResources() {
	}
}