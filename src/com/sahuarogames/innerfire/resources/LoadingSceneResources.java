package com.sahuarogames.innerfire.resources;

import org.andengine.audio.music.MusicFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;

public class LoadingSceneResources extends SceneResources {
	@Override
	public void loadResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		MusicFactory.setAssetBasePath("snd/");
	}

	@Override
	public TextureRegion getTextureRegion(String string) {
		return null;
	}

	@Override
	public void unloadResources() {
	}
}
