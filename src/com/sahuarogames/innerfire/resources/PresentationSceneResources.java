package com.sahuarogames.innerfire.resources;

import java.io.IOException;

import org.andengine.audio.music.MusicFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;

import com.sahuarogames.innerfire.BasicObjects;

public class PresentationSceneResources extends SceneResources {
	private BitmapTextureAtlas backgroundTextureAtlas;
	private TextureRegion backgroundTextureRegion;
	
	private BitmapTextureAtlas startOptionTextureAtlas;
	private TextureRegion startOptionTextureRegion;

	@Override
	public void loadResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/presentation_scene/");
		MusicFactory.setAssetBasePath("snd/presentation_scene/");		
		
		this.backgroundTextureAtlas = new BitmapTextureAtlas(BasicObjects.activity.getTextureManager(), 1024, 1024,
				TextureOptions.BILINEAR);
		this.backgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.backgroundTextureAtlas, BasicObjects.activity, "background.png", 0, 0);
		this.backgroundTextureAtlas.load();
		
		this.startOptionTextureAtlas = new BitmapTextureAtlas(BasicObjects.activity.getTextureManager(), 128, 128,
				TextureOptions.BILINEAR);
		this.startOptionTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				this.startOptionTextureAtlas, BasicObjects.activity, "startOption.png", 0, 0);
		this.startOptionTextureAtlas.load();

		try {
			this.backgroundMusic = MusicFactory.createMusicFromAsset(BasicObjects.activity.getMusicManager(),
					BasicObjects.activity, "bgm.mp3");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public TextureRegion getTextureRegion(String spriteName) {
		if (spriteName == "background") {
			return this.backgroundTextureRegion;
		} else if (spriteName == "startOption") {
			return this.startOptionTextureRegion;
		}
		
		return null;
	}

	@Override
	public void unloadResources() {
		this.backgroundTextureAtlas.unload();
		this.backgroundTextureAtlas = null;
		
		this.startOptionTextureAtlas.unload();
		this.startOptionTextureAtlas = null;
		
		this.backgroundMusic.stop();
		this.backgroundMusic = null;
	}
}
