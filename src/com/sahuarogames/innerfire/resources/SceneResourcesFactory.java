package com.sahuarogames.innerfire.resources;

import java.util.HashMap;
import java.util.Map;

import com.sahuarogames.innerfire.scenes.*;

public class SceneResourcesFactory {
	private static Map<Class<?>, Class<?>> sceneResources;

	static {
		sceneResources = new HashMap<Class<?>, Class<?>>();
		sceneResources.put(LoadingScene.class, LoadingSceneResources.class);
		sceneResources.put(PresentationScene.class, PresentationSceneResources.class);
		sceneResources.put(CharacterSelectionScene.class, CharacterSelectionSceneResources.class);
	}

	public static SceneResources getResources(Class<?> requestedClass) {
		Class<?> classToInstantiate = sceneResources.get(requestedClass);

		SceneResources resources = null;
		try {
			resources = (SceneResources) classToInstantiate.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		resources.loadResources();
		return resources;
	}
}
