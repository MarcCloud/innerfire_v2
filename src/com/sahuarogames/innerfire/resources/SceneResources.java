package com.sahuarogames.innerfire.resources;

import org.andengine.audio.music.Music;
import org.andengine.opengl.texture.region.TextureRegion;

public abstract class SceneResources {
	public Music backgroundMusic;
	
	public abstract void loadResources();

	public abstract void unloadResources();
	
	public abstract TextureRegion getTextureRegion(String string);
}
